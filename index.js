const express = require('express');
const path = require('path');
const fs = require('fs').promises;

const morgan = require('morgan');
const app = express();

const port = 8080;

const folderPath = path.join(__dirname, 'files');

const makeDir = () => {
  fs.mkdir(folderPath, { recursive: true });
};

app.use(morgan('combined'));
app.use(express.json()); // for parsing application/json

app.get('/api/files/:filename', async (req, res) => {
  try {
    makeDir();
    const fileName = req.params.filename;
    const filePath = path.join(folderPath, fileName);
    const fileContent = await fs.readFile(filePath, 'utf8');
    const fileExt = path.extname(filePath).substring(1);
    const stats = (await fs.stat(filePath)).birthtime;
    const resultObject = {
      message: 'Success',
      filename: fileName,
      content: fileContent,
      extension: fileExt,
      uploadedDate: stats,
    };
    res.status(200).json(resultObject);
  } catch (err) {
    // const errorObject = {
    //   message: `No file with ${fileName} filename found`,
    // };
    const fileName = req.params.filename;
    res.status(400).json({ message: `File '${fileName}' not found.` });
  }
});

app.get('/api/files', async (req, res) => {
  try {
    makeDir();
    const files = await fs.readdir(folderPath);
    const resultObject = {
      message: 'Success',
      files: files,
    };
    res.status(200).send(resultObject);
  } catch (err) {
    res.send(err);
  }
});

app.post('/api/files', async (req, res) => {
  makeDir();
  const { filename, content } = req.body;
  const filePath = path.join(folderPath, filename);
  await fs.writeFile(filePath, content, (err) => {
    if (err) {
      res.status(400).send({
        message: "Please specify 'content' parameter",
      });
    }
  });
  const resultObject = {
    message: 'File created successfully',
  };
  res.status(200).send(resultObject);
});

app.use((req, res) => {
  res.end('ROOT url');
});

app.listen(port);
